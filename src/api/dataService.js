

export default function() {
  return Promise.resolve(
    [
        {
          custid: 1,
          name: "Daniel",
          amt: 440,
          transactionDt: "09-11-2020"
        },
        {
          custid: 1,
          name: "Chris",
          amt: 725,
          transactionDt: "03-13-2020"
        },
        {
          custid: 1,
          name: "Amanda",
          amt: 424,
          transactionDt: "05-01-2021"
        },
        {
          custid: 1,
          name: "Jeniffer",
          amt: 180,
          transactionDt: "06-01-2019"
        },
        {
          custid: 1,
          name: "Murray",
          amt: 745,
          transactionDt: "06-21-2019"
        },
        {
          custid: 1,
          name: "SOfia",
          amt: 753,
          transactionDt: "07-01-2019"
        },
        {
          custid: 1,
          name: "Carlos",
          amt: 103,
          transactionDt: "07-14-2019"
        },
        {
          custid: 1,
          name: "Paul",
          amt: 850,
          transactionDt: "07-03-2019"
        },
        {
          custid: 1,
          name: "Antony",
          amt: 743,
          transactionDt: "07-21-2019"
        },
        {
          custid: 2,
          name: "Mary",
          amt: 997,
          transactionDt: "05-01-2019"
        },
        {
          custid: 2,
          name: "Thompson",
          amt: 325,
          transactionDt: "05-21-2019"
        },
        {
          custid: 2,
          name: "Marshall",
          amt: 754,
          transactionDt: "02-01-2019"
        },
        {
          custid: 2,
          name: "Sandra",
          amt: 562,
          transactionDt: "01-21-2019"
        },
        {
          custid: 2,
          name: "Roy",
          amt: 871,
          transactionDt: "07-01-2019"
        },
        {
          custid: 2,
          name: "Brown",
          amt: 646,
          transactionDt: "09-21-2019"
        },
        {
          custid: 3,
          name: "James",
          amt: 539,
          transactionDt: "06-21-2020"
        }
    ]
  );
};